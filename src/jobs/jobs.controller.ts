import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UsePipes,
  UseGuards,
  Query,
} from '@nestjs/common';
import { JobsService } from './jobs.service';
import { CreateJobDto } from './dto/create-job.dto';
import { UpdateJobDto } from './dto/update-job.dto';
import { ValidationPipe } from 'src/shared/validation.pipe';
import { AuthGuard } from 'src/shared/auth.guard';
import { UserDecorator } from 'src/users/users.decorator';

@Controller('api/jobs')
export class JobsController {
  constructor(private readonly jobsService: JobsService) {}

  @Post()
  @UsePipes(new ValidationPipe())
  @UseGuards(new AuthGuard())
  create(@UserDecorator('id') user, @Body() createJobDto: CreateJobDto) {
    return this.jobsService.create(user, createJobDto);
  }

  @Get()
  @UseGuards(new AuthGuard())
  findAll(@Query('page') page: number) {
    return this.jobsService.findAll(page);
  }
  
  @Get('/newest')
  @UseGuards(new AuthGuard())
  showNewest(@Query('page') page: number) {
    return this.jobsService.findAll(page, true);
  }

  @Get(':id')
  @UseGuards(new AuthGuard())
  findOne(@Param('id') id: string) {
    return this.jobsService.findOne(id);
  }

  @Patch(':id')
  @UseGuards(new AuthGuard())
  @UsePipes(new ValidationPipe())
  update(
    @Param('id') id: string,
    @UserDecorator('id') user: string,
    @Body() updateJobDto: Partial<UpdateJobDto>,
  ) {
    return this.jobsService.update(id, user, updateJobDto);
  }

  @Post(':id/upvote')
  @UseGuards(new AuthGuard())
  upvoteJob(@Param('id') id: string, @UserDecorator('id') userId: string) {
    return this.jobsService.upvoteJob(id, userId);
  }

  @Post(':id/downvote')
  @UseGuards(new AuthGuard())
  downvoteJob(@Param('id') id: string, @UserDecorator('id') userId: string){
    return this.jobsService.downvoteJob(id, userId);
  }

  @Delete(':id')
  @UseGuards(new AuthGuard())
  remove(@Param('id') id: string, @UserDecorator('id') user: string) {
    return this.jobsService.remove(id, user);
  }

  @Post(':id/bookmark')
  @UseGuards(new AuthGuard())
  bookmarkJob(@Param('id') id: string, @UserDecorator('id') user: string) {
    return this.jobsService.bookmarkJob(id, user);
  }

  @Delete(':id/bookmark')
  @UseGuards(new AuthGuard())
  unbookmarkJob(@Param('id') id: string, @UserDecorator('id') user: string) {
    return this.jobsService.unbookmarkJob(id, user);
  }
}
