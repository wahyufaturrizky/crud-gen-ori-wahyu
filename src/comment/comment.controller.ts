import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  UsePipes,
  ValidationPipe,
  Query,
} from '@nestjs/common';
import { AuthGuard } from 'src/shared/auth.guard';
import { UserDecorator } from 'src/users/users.decorator';
import { CommentService } from './comment.service';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';

@Controller('api/comment')
export class CommentController {
  constructor(private readonly commentService: CommentService) {}

  @Post('job/:id')
  @UsePipes(new ValidationPipe())
  @UseGuards(new AuthGuard())
  createComment(
    @Param('id') jobId: string,
    @Body() createCommentDto: CreateCommentDto,
    @UserDecorator('id') userId,
  ) {
    return this.commentService.createComment(jobId, createCommentDto, userId);
  }

  @Get()
  @UseGuards(new AuthGuard())
  findAll() {
    return this.commentService.findAll();
  }

  @Get(':id')
  @UseGuards(new AuthGuard())
  findOne(@Param('id') id: string) {
    return this.commentService.findOne(id);
  }

  @Get('job/:id')
  @UseGuards(new AuthGuard())
  findCommentByJob(@Param('id') id: string, @Query('page') page: number) {
    return this.commentService.findCommentByJob(id, page);
  }

  @Get('user/:id')
  @UseGuards(new AuthGuard())
  findCommentByUser(@Param('id') id: string, @Query('page') page: number) {
    return this.commentService.findCommentByUser(id, page);
  }

  @Patch(':id')
  @UseGuards(new AuthGuard())
  @UsePipes(new ValidationPipe())
  update(
    @Param('id') id: string,
    @Body() updateCommentDto: UpdateCommentDto,
    @UserDecorator('id') userId,
  ) {
    return this.commentService.update(id, updateCommentDto, userId);
  }

  @Delete(':id')
  @UseGuards(new AuthGuard())
  remove(@Param('id') id: string, @UserDecorator('id') userId) {
    return this.commentService.remove(id, userId);
  }
}
