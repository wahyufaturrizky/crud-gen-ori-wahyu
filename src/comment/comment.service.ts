import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Job } from 'src/jobs/entities/job.entity';
import { User } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';
import { CreateCommentDto, CreateCommentRo } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';
import { Comment } from './entities/comment.entity';

@Injectable()
export class CommentService {
  constructor(
    @InjectRepository(Comment) private commentRepository: Repository<Comment>,
    @InjectRepository(User) private userRepository: Repository<User>,
    @InjectRepository(Job) private jobRepository: Repository<Job>,
  ) {}

  private toResponseObject(comment: Comment): CreateCommentRo {
    const response: any = {
      ...comment,
      user: comment.user.toResponseObject(false),
    };

    return response;
  }

  private ensureOwnership(data: Comment, userId: string) {
    if (data.user.id !== userId) {
      throw new HttpException('Incorect user', HttpStatus.UNAUTHORIZED);
    }
  }

  async createComment(
    jobId: string,
    createCommentDto: CreateCommentDto,
    id: string,
  ): Promise<CreateCommentRo> {
    const job = await this.jobRepository.findOne({ where: { id: jobId } });
    const user = await this.userRepository.findOne({ where: { id } });

    const responseComment = await this.commentRepository.create({
      ...createCommentDto,
      job,
      user,
    });

    await this.commentRepository.save(responseComment);
    return this.toResponseObject(responseComment);
  }

  async findAll(): Promise<CreateCommentRo[]> {
    const res = await this.commentRepository.find({
      relations: ['user', 'job'],
    });

    if (!res) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    return res.map((data) => this.toResponseObject(data));
  }

  async findOne(id: string): Promise<CreateCommentRo> {
    const res = await this.commentRepository.findOne({
      where: { id },
      relations: ['user', 'job'],
    });

    if (!res) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    return this.toResponseObject(res);
  }

  async findCommentByJob(id: string, page = 1) {
    const res = await this.commentRepository.find({
      where: { job: { id } },
      relations: ['user', 'job'],
      take: 10,
      skip: 10 * (page - 1),
    });

    if (!res) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    if (!(res && res[0])) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    return res.map((data) => this.toResponseObject(data));
  }

  async findCommentByUser(id: string, page = 1) {
    const res = await this.commentRepository.find({
      where: { user: { id } },
      relations: ['user', 'job'],
      take: 10,
      skip: 10 * (page - 1),
    });

    if (!res) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    return res.map((data) => this.toResponseObject(data));
  }

  async update(id: string, updateCommentDto: UpdateCommentDto, userId: string) {
    let res = await this.commentRepository.findOne({
      where: { id },
      relations: ['user'],
    });

    if (!res) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    this.ensureOwnership(res, userId);
    await this.commentRepository.update({ id }, updateCommentDto);
    res = await this.commentRepository.findOne({
      where: { id },
      relations: ['user'],
    });
    return this.toResponseObject(res);
  }

  async remove(id: string, userId: string) {
    const res = await this.commentRepository.findOne({
      where: { id },
      relations: ['user', 'job'],
    });

    if (!res) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    this.ensureOwnership(res, userId);
    await this.commentRepository.remove(res);
    return this.toResponseObject(res);
  }
}
